import os

# 输出当前路径
print(os.getcwd())

# 连接文件夹路径
print(os.path.join("project","python"))

# 以列表形式输出当前目录下所有文件和文件夹，可以在listdir()中指定目录
print(os.listdir())

# 判断是否是文件夹
for file in os.listdir():
	print(file,os.path.isdir(file))

for file in os.scandir():
	print(file.name,file.path,file.is_dir())
	# 输出文件名称，相对路径，是否是文件夹