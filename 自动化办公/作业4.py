import os
import shutil
import glob
import time

if not os.path.exists("backup"):
	os.mkdir("backup")


for file in os.scandir():
	if file.name.endswith(".zip"):
		timestamp = file.stat().st_mtime
		time_locla = time.localtime(timestamp)
		newtime = time.strftime("%Y-%m-%d",time_locla)
		newname = newtime+"-"+file.name
		os.rename(file.name,newname)
		shutil.move(newname,"./backup/")