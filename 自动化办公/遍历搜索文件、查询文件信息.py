import os
import glob
import fnmatch
import datetime

# 文件夹路径，文件夹名称，文件名称列表
for dirpath,dirnames,fielnames in os.walk("./"):
    print(f"发现文件夹{dirpath}")
    print(fielnames)

# 检测字符串a是否以字符串b开头
print("test.py".startswith("test"))
# 检测字符串a是否以字符串b结尾
print("test.py".endswith(".txt"))

# *代表任意字符
# recursive代表更深入的遍历文件夹
print(glob.glob("**/*.py",recursive=True))

# 判断参数1是否符合参数2的要求
print(fnmatch.fnmatch("test.py","te*.py"))

# 文件信息
for file in os.scandir():
    print(file.name,file.stat())

# 转化Unix时间戳
that_time = datetime.datetime.fromtimestamp(15886799025)
# 直接输出日期
print(that_time)
# 输出小时
print(that_time.hour)

# 查看指定文件信息
print(os.stat('自动化办公/test.py'))