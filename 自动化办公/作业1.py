import os

sum = 0
file_sum = 0

for file in os.scandir():
	# 判断是文件并且名字中包含python
	if file.is_file and ("python" in file.name.lower()) :
		sum += 1
	# 判断是文件
	elif file.is_file:
		file_sum += 1

# 格式化输出
print("当前目录下名字中包含python的文件数量{},文件数量{}".format(sum,file_sum))